function [ fixationData ] = getFixationData( data )

    PIXEL_JUMP_LIMIT = 50;
    
    X = 1;
    Y = 2;
    
    GX = 3;
    GY = 4;
    
    TIME_SPENT = 3;
    
    [sizeX, sizeY] = size(data);
    
    fixationData = [];
    
    lastPoint = [data(1, GX), data(1, GY)];
    for i=1:sizeX
        actualPoint = [data(i, GX), data(i, GY)];
        if (spaceBetween(lastPoint, actualPoint) > PIXEL_JUMP_LIMIT)
            fixationData = [fixationData; data(i, 2:end)];
        end
        lastPoint = actualPoint;
    end
        


end

