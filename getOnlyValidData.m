function [ filtredData ] = getOnlyValidData( step )
    G_STEP = 1;
    unfiltredData = csvread('test_1/gazedata.csv', 1, 0);
    
    filtredData = [];
    [sizeX, sizeY] = size(unfiltredData);

    for i = 1:1:sizeX
        if (unfiltredData(i, G_STEP) == step && isInRangeOfFirstImg(unfiltredData(i, :)))
            filtredData = [filtredData; unfiltredData(i, :)];
        elseif (unfiltredData(i, G_STEP) == step && isInRangeOfSecondImg(unfiltredData(i, :)))
            filtredData = [filtredData; unfiltredData(i, :)];
        elseif (unfiltredData(i, G_STEP) > step)
            break
        end
    end
end

