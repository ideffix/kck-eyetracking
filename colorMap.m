function [ coloredMap ] = colorMap( map )
    [sizeX, sizeY] = size(map);
    coloredMap = zeros(sizeX, sizeY, 3);
    maxValue = max(max(map));
    
    FIRST_LEVEL = round(maxValue / 4);
    SECOND_LEVEL = round(maxValue / 2);
    THRID_LEVEL = round(maxValue * 0.75);
    
    RED = 1;
    GREEN = 2;
    BLUE = 3;
    
    for i=1:sizeX
        for j=1:sizeY
            if (map(i, j) < FIRST_LEVEL)
                coloredMap(i, j, BLUE) = 255;
                coloredMap(i, j, GREEN) = round((map(i, j) / FIRST_LEVEL) * 255);
            elseif (map(i, j) < SECOND_LEVEL)
                coloredMap(i, j, GREEN) = 255;
                coloredMap(i, j, BLUE) = 255 - round(((map(i, j) - FIRST_LEVEL) / (SECOND_LEVEL - FIRST_LEVEL)) * 255);     
            elseif (map(i, j) < THRID_LEVEL)
                coloredMap(i, j, GREEN) = 255;
                coloredMap(i, j, RED) = round(((map(i, j) - SECOND_LEVEL) / (THRID_LEVEL - SECOND_LEVEL)) * 255);
            else 
                coloredMap(i, j, RED) = 255;
                coloredMap(i, j, GREEN) = 255 - round(((map(i, j) - THRID_LEVEL) / (maxValue - THRID_LEVEL)) * 255);
            end             
        end
    end


end

