function [ map ] = applyMask( map, midPoint, mask )
    [maskSizeX, maskSizeY] = size(mask);
    maskMidX = ceil(maskSizeX / 2);
    maskMidY = ceil(maskSizeY / 2);
    
    for i=1:maskSizeX
        for j=1:maskSizeY
            map(midPoint(1) - maskMidX - 2 + i, midPoint(2) - maskMidY - 2 + j) = map(midPoint(1) - maskMidX - 2 + i, midPoint(2) - maskMidY - 2 + j) + mask(i, j);
        end
    end

end

