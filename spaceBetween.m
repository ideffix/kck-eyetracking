function [ space ] = spaceBetween( M1, M2 )
    space = sqrt((M2(1) - M1(1))^2 - (M2(2) - M1(2))^2);
end

