function [ data ] = removeShortDurationEntities( data )
    
    SHORT_TIME = 50;
    TIME_DURATION_INDEX = 4;
    [sizeX, sizeY] = size(data);
    i = 1;
    while (i <= sizeX)
        if (data(i, TIME_DURATION_INDEX) < SHORT_TIME)
            data (i, :) = [];
            sizeX = sizeX - 1;
        else
            i = i + 1;
        end
    end


end

