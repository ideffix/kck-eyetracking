function [ M ] = drawPointsOnImage( img, binary )

    [sizeX, sizeY] = size(binary);
    M = img;
    for i=1:1:sizeX
        for j=1:1:sizeY
            if (binary(i, j) == 1)
                M(i, j, 2) = 255;
            end
        end
    end

end

