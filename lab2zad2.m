gazedata = csvread('test_1/gazedata.csv', 1, 0);

imgName = 'c045f.jpg';
imgPath = strcat('baza_biut/', imgName);

mask = [1, 3, 1;
        3, 5, 3;
        1, 3, 1];
img = imread(imgPath);
[step, imgNumb] = findStepAndImgNumber(imgName);
%------

data = filterData(gazedata, step, imgNumb);
%binaryM = hitPoints(data, imgNumb);
heatMap = createHeatMap(data, mask, imgNumb);
coloredHeatMap = uint8(colorMap(heatMap));
subplot(1, 2, 1);
imshow(coloredHeatMap);
subplot(1, 2, 2);
imshow(img);
%subplot(1, 3, 3);
%imshow(drawPointsOnImage(img, binaryM));