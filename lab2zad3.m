gazedata = csvread('test_1/gazedata.csv', 1, 0);
sessions = importdata('test_1/sessions.csv');

FIRST_IMG_INDEX = 4;
SECOND_IMG_INDEX = 10;

[sizeX, sizeY] = size(sessions.textdata);

mask = [1, 3, 1;
        3, 5, 3;
        1, 3, 1];

globalHeatMap = zeros(400, 300);
img = imread('baza_biut/a020f.jpg');
for i=2:sizeX
    imgPath = cell2mat(sessions.textdata(i, FIRST_IMG_INDEX));
    [step, imgNumb] = findStepAndImgNumber(imgPath);
    data = filterData(gazedata, step, imgNumb);
    heatMap = createHeatMap(data, mask, imgNumb);
    globalHeatMap = globalHeatMap + heatMap;
       
    imgPath = cell2mat(sessions.textdata(i, SECOND_IMG_INDEX));
    [step, imgNumb] = findStepAndImgNumber(imgPath);
    data = filterData(gazedata, step, imgNumb);
    heatMap = createHeatMap(data, mask, imgNumb);
    globalHeatMap = globalHeatMap + heatMap;
end


imshow(uint8(colorMap(globalHeatMap)));


    

