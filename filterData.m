function [ filtredData ] = filterData( unfiltredData, step, imgNumb )
    G_STEP = 1;
    
    filtredData = [];
    [sizeX, sizeY] = size(unfiltredData);

    for i = 1:1:sizeX
        if (imgNumb == 1 && unfiltredData(i, G_STEP) == step && isInRangeOfFirstImg(unfiltredData(i, :)))
            filtredData = [filtredData; unfiltredData(i, :)];
        elseif (imgNumb == 2 && unfiltredData(i, G_STEP) == step && isInRangeOfSecondImg(unfiltredData(i, :)))
            filtredData = [filtredData; unfiltredData(i, :)];
        elseif (unfiltredData(i, G_STEP) > step)
            break
        end
    end
end

