gazedata = csvread('test_1/gazedata.csv', 1, 0);
%sessions = csvread('test_1/sessions.csv', 1, 0);

imgName = 'c049f.jpg';
imgPath = strcat('baza_biut/', imgName);
img = imread(imgPath);
[step, imgNumb] = findStepAndImgNumber(imgName);
%------

data = filterData(gazedata, step, imgNumb);
binaryM = hitPoints(data, imgNumb);
subplot(1, 3, 1);
imshow(binaryM);
subplot(1, 3, 2);
imshow(img);
subplot(1, 3, 3);
imshow(drawPointsOnImage(img, binaryM));

