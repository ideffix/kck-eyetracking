function [ M ] = rescaleDown( matrix, scale )

M = matrix(1:scale:end, 1:scale:end);

end

