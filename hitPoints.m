function [ map ] = hitPoints( pointsInRange ,imgNumb )
    G_X = 3;
    G_Y = 4;
    
    xOffset = 189;
    yOffset = 124;
    if (imgNumb == 2)
        xOffset = 889;
    end
    map = zeros(400, 300);
    [sizeX, sizeY] = size(pointsInRange);
    for i = 1:1:sizeX
        map(round((pointsInRange(i, G_Y) - yOffset) / 2), round((pointsInRange(i, G_X) - xOffset) / 2)) = 1;
    end
end

