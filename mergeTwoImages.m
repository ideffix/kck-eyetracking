function [ mergedImg ] = mergeTwoImages( firstImg, secondImg )
    mergedImg = [firstImg, ones(400, 50, 3) .* 255, secondImg];
end

