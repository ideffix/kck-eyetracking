function [ data ] = countDurationTime( data )
    [sizeX, sizeY] = size(data);
    data(1, 4) = data(1, 1);
    for i=2:sizeX
        data(i, 4) = data(i, 1) - data(i - 1, 1);
    end
end

