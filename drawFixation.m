function [] = drawFixation( img, fixationData )
    imshow(img)
%     centx = 50;
%     centy = 50;
    TIME_DURATION_INDEX = 4;
    X = 2;
    Y = 3;
    xOffset = 189;
    yOffset = 124;
    
    [sizeX, sizeY] = size(fixationData);
    MAX_R = 50;
    MAX_DURATION = max(fixationData(:, TIME_DURATION_INDEX));
    theta = 0 : (2 * pi / 10000) : (2 * pi);
    
    
    hold on;
    for i=1:sizeX;
        r = round((fixationData(i, TIME_DURATION_INDEX) / MAX_DURATION) * MAX_R);
        x = round((fixationData(i, X) - xOffset) / 2);
        y = round((fixationData(i, Y) - yOffset) / 2);
        pline_x = r * cos(theta) + x;
        pline_y = r * sin(theta) + y;
        k = ishold;
        plot(pline_x, pline_y, 'r-', 'LineWidth', 3);
        text(x, y, num2str(i));
        if (i > 1)
            xlast = round((fixationData(i - 1, X) - xOffset) / 2);
            ylast = round((fixationData(i - 1, Y) - yOffset) / 2);
            line([x, xlast],[y,ylast],'Color','b','LineWidth',1);
        end
        
    end
    hold off;

end

