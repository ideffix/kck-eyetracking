function [ isInRange ] = isInRangeOfSecondImg( row )
    G_X = 3;
    G_Y = 4;
    
    S_AX0 = 890;
    S_AY0 = 125;
    S_AX1 = 1490;
    S_AY1 = 925;
    
    isInRange = false;
    
    if (row(G_X) >= S_AX0 && row(G_X) <= S_AX1 ...
            && row(G_Y) >= S_AY0 && row(G_Y) <= S_AY1)
        isInRange = true;
    end


end

