function [ step, imgNumber ] = findStepAndImgNumber( imgName )

    step = 0;
    imgNumber = 0;
    FIRST_IMG_INDEX = 4;
    SECOND_IMG_INDEX = 10;
    STEP_INDEX = 2;

    %commentet it for zad3, uncomment for zad1 and 2
    imgName = strcat('img_rank/', imgName);
    sessions = importdata('test_1/sessions.csv');
    [sizeX, sizeY] = size(sessions.textdata);
    
    for i=2:sizeX
        if (strcmp(sessions.textdata(i, FIRST_IMG_INDEX), imgName))
            step = str2num(cell2mat(sessions.textdata(i, STEP_INDEX)));
            imgNumber = 1;
            break;
        elseif (strcmp(sessions.textdata(i, SECOND_IMG_INDEX), imgName))
            step = str2num(cell2mat(sessions.textdata(i, STEP_INDEX)));
            imgNumber = 2;
            break;
        end
        


end

