function [ map ] = createHeatMap( pointsInRange , mask, imgNumb )
    G_X = 3;
    G_Y = 4;
    
    [maskSizeX, maskSizeY] = size(mask);
    xOffset = 189;
    yOffset = 124;
    if (imgNumb == 2)
        xOffset = 889;
    end
    map = zeros(2 * maskSizeX + 400, 2 * maskSizeY + 300);
    [sizeX, sizeY] = size(pointsInRange);
    for i = 1:1:sizeX
        midPoint = [round((pointsInRange(i, G_Y) - yOffset) / 2) + maskSizeY, round((pointsInRange(i, G_X) - xOffset) / 2) + maskSizeX];
        map = applyMask(map, midPoint, mask);
    end
    
    map = map(maskSizeX + 1: end-maskSizeX, maskSizeY + 1 : end-maskSizeY);

end

