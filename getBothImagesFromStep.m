function [ firstImg, secondImg ] = getBothImagesFromStep( step )

    FIRST_IMG_INDEX = 4;
    SECOND_IMG_INDEX = 10;
    STEP_INDEX = 2;

    %commentet it for zad3, uncomment for zad1 and 2
    %imgName = strcat('img_rank/', imgName);
    sessions = importdata('test_1/sessions.csv');
    [sizeX, sizeY] = size(sessions.textdata);
    for i=2:sizeX
        if (str2num(cell2mat(sessions.textdata(i, STEP_INDEX))) == step)
            firstImg = imread(cell2mat(sessions.textdata(i, FIRST_IMG_INDEX)));
            secondImg = imread(cell2mat(sessions.textdata(i, SECOND_IMG_INDEX)));
            break;
        end
    end


end

