STEP = 8;

[firstImg, secondImg] = getBothImagesFromStep(STEP);
mergedImages = mergeTwoImages(firstImg, secondImg);
data = getOnlyValidData(STEP);
fixationData = getFixationData(data); 
fixationData = countDurationTime(fixationData);
fixationData = removeShortDurationEntities(fixationData);
drawFixation(mergedImages, fixationData)


